DMenu config
====================
My patched DMenu


Patches
------------
- alpha
- border
- center
- (colored caret)
- fuzzyhighlight
- fuzzymatch
- (line height)
- (lines below prompt)
- (numbers)


Installation
------------
```sh
sudo make clean install
```


Run
---
```sh
dmenu_run -c -l 15 -i -sb "#5B068D" -mb "#2F2240" -shb "#5B068D"
```
